package ru.toliklunev;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Supplier;

public final class EntityLocker {
    private final static Map<? super Object, EntityLock> ENTITY_LOCK_MAP = new ConcurrentHashMap<>();

    private final static Map<EntityLock, Thread> OWNED_MAP = new ConcurrentHashMap<>();
    private final static Map<Thread, EntityLock> WANTED_MAP = new ConcurrentHashMap<>();

    private final static ReentrantReadWriteLock glob = new ReentrantReadWriteLock();

    public static void executeGlobal(Duration duration, Runnable runnable) throws InterruptedException {
        boolean isLocked = glob.writeLock().tryLock(duration.toNanos(), TimeUnit.NANOSECONDS);

        if (!isLocked) {
            throw new LockException();
        }

        try {
            runnable.run();
        } finally{
            glob.writeLock().unlock();
        }
    }

    public static <PK, T> T executeRead(PK pk, Supplier<T> supplier) {
        EntityLock lock = wrap(pk);
        lock.lockRead();
        try {
            return supplier.get();
        } finally {
            lock.unlockRead();
        }
    }

    public static <PK, T> T executeRead(PK pk, Duration duration, Supplier<T> supplier) throws InterruptedException {
        EntityLock lock = wrap(pk);
        boolean isLocked = lock.lockRead(duration.toNanos(), TimeUnit.NANOSECONDS);

        if (!isLocked) {
            throw new LockException();
        }

        try {
            return supplier.get();
        } finally {
            lock.unlockRead();
        }
    }

    public static <PK> void executeWrite(PK pk, Runnable runnable) {
        executeWrite(pk, runnable, false);
    }

    public static <PK> void executeWrite(PK pk, Runnable runnable, boolean avoidDeadlock) {
        EntityLock lock = wrap(pk);
        lock.lockWrite(avoidDeadlock);

        try {
            runnable.run();
        } finally {
            lock.unlockWrite();
        }
    }

    public static <PK> void executeWrite(PK pk, Duration duration, Runnable runnable) throws InterruptedException {
        EntityLock lock = wrap(pk);
        boolean isLocked = lock.lockWrite(duration.toNanos(), TimeUnit.NANOSECONDS);

        if (!isLocked) {
            throw new LockException();
        }

        try {
            runnable.run();
        } finally {
            lock.unlockWrite();
        }
    }

    public static <PK> EntityLock wrap(PK pk) {
        return ENTITY_LOCK_MAP.computeIfAbsent(pk, (k) -> new EntityLock());
    }

    public static void clear() {
        ENTITY_LOCK_MAP.clear();
        OWNED_MAP.clear();
        WANTED_MAP.clear();
    }

    private static synchronized void checkDeadlock(EntityLock t) {
        EntityLock res = t;

        if (Thread.currentThread().equals(OWNED_MAP.get(res))) {
            return;
        }

        while (true) {
            Thread thread = OWNED_MAP.get(res);
            if (thread == null) {
                break;
            }

            if (Thread.currentThread().equals(thread)) {
                throw new DeadlockAvoidException();
            }

            res = WANTED_MAP.get(thread);
            if (res == null) {
                break;
            }
        }

        WANTED_MAP.put(Thread.currentThread(), t);
    }

    public final static class EntityLock {
        private final Lock rLock;
        private final Lock rwLock;

        public EntityLock() {
            ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
            this.rLock = lock.readLock();
            this.rwLock = lock.writeLock();
        }

        public void lockRead() {
            glob.readLock().lock();
            rLock.lock();
        }

        public boolean lockRead(long time, TimeUnit unit) throws InterruptedException {

            long limit = System.nanoTime() + unit.toNanos(time);

            boolean locked = glob.readLock().tryLock(time, unit);
            if (!locked) {
                return false;
            }

            if (limit >= System.nanoTime()) {
                return false;
            }

            return rLock.tryLock(limit - System.nanoTime(), TimeUnit.NANOSECONDS);
        }

        public void unlockRead() {
            ENTITY_LOCK_MAP.remove(this);
            rLock.unlock();
            glob.readLock().unlock();
        }

        public void lockWrite() {
            lockWrite(false);
        }

        public void lockWrite(boolean avoidDeadlock) {
            glob.readLock().lock();
            if (avoidDeadlock) {
                checkDeadlock(this);
            }

            rwLock.lock();

            WANTED_MAP.remove(Thread.currentThread());
            OWNED_MAP.put(this, Thread.currentThread());
        }

        public boolean lockWrite(long time, TimeUnit unit) throws InterruptedException {
            long limit = System.nanoTime() + unit.toNanos(time);

            boolean locked = glob.readLock().tryLock(time, unit);
            if (!locked) {
                return false;
            }

            if (limit >= System.nanoTime()) {
                glob.readLock().unlock();
                return false;
            }

            WANTED_MAP.put(Thread.currentThread(), this);

            locked = rwLock.tryLock(limit - System.nanoTime(), TimeUnit.NANOSECONDS);
            if (locked) {
                OWNED_MAP.put(this, Thread.currentThread());
            } else {
                glob.readLock().unlock();
            }

            WANTED_MAP.remove(Thread.currentThread());
            return locked;
        }

        public void unlockWrite() {
            OWNED_MAP.remove(this);
            ENTITY_LOCK_MAP.remove(this);
            rwLock.unlock();
            glob.readLock().unlock();
        }
    }

    public static class LockException extends RuntimeException {
    }

    public static class DeadlockAvoidException extends RuntimeException {
    }
}
