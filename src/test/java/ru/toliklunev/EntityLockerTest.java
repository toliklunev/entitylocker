package ru.toliklunev;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class EntityLockerTest {

    @BeforeEach
    public void setUp() {
        EntityLocker.clear();
    }

    @Test
    public void justLockTest() {
        Object pk = new Object();
        EntityLocker.executeWrite(pk, () -> {
        });
    }

    @Test
    public void differentPksTest() throws Exception {
        CountDownLatch countDownLatch1 = new CountDownLatch(2);
        CountDownLatch countDownLatch2 = new CountDownLatch(1);

        Object pk1 = new Object();
        Object pk2 = new Object();

        new Thread(() -> EntityLocker.executeWrite(pk1, () -> {
            countDownLatch1.countDown();
            try {
                countDownLatch2.await();
                countDownLatch1.countDown();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        })).start();

        EntityLocker.executeWrite(pk2, countDownLatch2::countDown);
        countDownLatch1.await();
    }

    @Test
    public void tryLockTest() throws Exception {
        Object pk = new Object();

        CountDownLatch countDownLatch1 = new CountDownLatch(1);
        CountDownLatch countDownLatch2 = new CountDownLatch(1);

        new Thread(() -> EntityLocker.executeWrite(pk, () -> {
            countDownLatch1.countDown();
            try {
                countDownLatch2.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        })).start();

        countDownLatch1.await();

        assertThrows(EntityLocker.LockException.class, () ->
            EntityLocker.executeWrite(pk, Duration.ZERO, () -> {}));

        countDownLatch2.countDown();
    }

    @Test
    public void globLockTest() throws Exception {
        Object pk = new Object();
        CountDownLatch countDownLatch1 = new CountDownLatch(1);
        CountDownLatch countDownLatch2 = new CountDownLatch(1);

        new Thread(() -> {
            try {
                EntityLocker.executeGlobal(Duration.ZERO, () -> {
                    try {
                        countDownLatch1.countDown();
                        countDownLatch2.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        countDownLatch1.await();

        assertThrows(EntityLocker.LockException.class, () ->
            EntityLocker.executeWrite(pk, Duration.ZERO, () -> {}));

        countDownLatch2.countDown();
    }

    @Test
    public void deadlockDetectionTest() throws Exception {
        Object pk1 = new Object();
        Object pk2 = new Object();

        CountDownLatch countDownLatch1 = new CountDownLatch(1);
        CountDownLatch countDownLatch2 = new CountDownLatch(1);

        new Thread(() -> EntityLocker.executeWrite(pk1, () -> {
            countDownLatch1.countDown();

            try {
                countDownLatch2.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            EntityLocker.wrap(pk2).lockWrite(true);
            EntityLocker.wrap(pk2).unlockWrite();
        })).start();

        countDownLatch1.await();

        EntityLocker.executeWrite(pk2, () -> {
            countDownLatch2.countDown();

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            assertThrows(EntityLocker.DeadlockAvoidException.class, () ->
                EntityLocker.wrap(pk1).lockWrite(true));
        });
    }

    @Test
    public void reentrancyTest() throws Exception {
        Object pk1 = new Object();
        EntityLocker.wrap(pk1).lockWrite(false);
        EntityLocker.wrap(pk1).lockWrite(true);
        EntityLocker.wrap(pk1).lockWrite(true);
        EntityLocker.wrap(pk1).unlockWrite();
        EntityLocker.wrap(pk1).unlockWrite();
    }

}
